# Homework - Banking solution
Simple example to showcase my programming abilities using Java and Spring boot stack. Database is in-memory H2. 
Data layer uses Spring Data/Jpa(Hibernate).

## Running the application
Starting spring boot application server using `bootRun` gradle task
```
gradlew bootRun
```
Running integration tests using `bootTestRun` gradle task
```
gradlew bootTestRun
```

## About the project
The HTTP endpoints are not secured and uses a default hardcoded user for actions. Http endpoints are defined in `AccountController` and `TransactionController`. 

Both of the controllers are covered by integration tests. Due to small scope of example unit tests are absent 
and would duplicate integration tests. 
