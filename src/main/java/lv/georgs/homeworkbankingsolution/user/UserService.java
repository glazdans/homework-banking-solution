package lv.georgs.homeworkbankingsolution.user;

import lombok.AllArgsConstructor;
import lv.georgs.homeworkbankingsolution.model.BankUser;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public BankUser getCurrentUser() {
        return userRepository.findById(PrototypeUserData.DEFAULT_USER_ID)
                .orElseThrow();
    }

}
