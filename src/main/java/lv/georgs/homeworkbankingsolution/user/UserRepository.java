package lv.georgs.homeworkbankingsolution.user;

import lv.georgs.homeworkbankingsolution.model.BankUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<BankUser, Long> {
}
