package lv.georgs.homeworkbankingsolution.user;

import lombok.AllArgsConstructor;
import lv.georgs.homeworkbankingsolution.model.BankUser;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PrototypeUserData implements CommandLineRunner {
    public static final Long DEFAULT_USER_ID = 1L;

    private final UserRepository userRepository;
    @Override
    public void run(String... args) {
        BankUser bankUser = new BankUser();
        bankUser.setId(DEFAULT_USER_ID);
        bankUser.setName("Georgs");
        userRepository.save(bankUser);
    }
}
