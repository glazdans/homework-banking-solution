package lv.georgs.homeworkbankingsolution.model;

import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private TransactionType transactionType;
    @ManyToOne
    @Nullable
    private Account sourceAccount;
    @ManyToOne
    @Nullable
    private Account targetAccount;
    private BigDecimal amount;
    private OffsetDateTime transactionTime;
}
