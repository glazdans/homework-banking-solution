package lv.georgs.homeworkbankingsolution.model;

public enum TransactionType {
    WITHDRAW, DEPOSIT, TRANSFER
}
