package lv.georgs.homeworkbankingsolution.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToOne
    private BankUser bankUser;
    private String accountNumber;
    private OffsetDateTime creationDate;
}
