package lv.georgs.homeworkbankingsolution.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class BankUser {
    @Id
    private Long id;
    private String name;
    @OneToMany(mappedBy = "bankUser")
    private List<Account> accountList;
}
