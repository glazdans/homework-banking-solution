package lv.georgs.homeworkbankingsolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkBankingSolutionApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeworkBankingSolutionApplication.class, args);
	}

}
