package lv.georgs.homeworkbankingsolution.transaction;

import lombok.AllArgsConstructor;
import lv.georgs.homeworkbankingsolution.model.Account;
import lv.georgs.homeworkbankingsolution.model.Transaction;
import lv.georgs.homeworkbankingsolution.model.TransactionType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Objects;

@Service
@AllArgsConstructor
public class TransactionService {
    private final TransactionRepository transactionRepository;

    @Transactional
    public void deposit(Account account, BigDecimal amount) {
        validateAmount(amount);

        var transaction = createTransaction(TransactionType.DEPOSIT);
        transaction.setTargetAccount(account);
        transaction.setAmount(amount);
        transactionRepository.save(transaction);
    }

    @Transactional
    public void withdraw(Account account, BigDecimal amount) {
        validateAmount(amount);
        checkAccountBalance(account, amount);

        var transaction = createTransaction(TransactionType.WITHDRAW);
        transaction.setSourceAccount(account);
        transaction.setAmount(amount);
        transactionRepository.save(transaction);
    }

    @Transactional
    public void transfer(Account source, Account target, BigDecimal amount) {
        validateAmount(amount);
        checkAccountBalance(source, amount);

        var transaction = createTransaction(TransactionType.TRANSFER);
        transaction.setSourceAccount(source);
        transaction.setTargetAccount(target);
        transaction.setAmount(amount);
        transactionRepository.save(transaction);
    }

    public BigDecimal getAccountBalance(Account account) {
        var accountTransactions = transactionRepository
                .findBySourceAccountOrTargetAccountOrderByTransactionTime(account, account);

        var outgoingAmount = accountTransactions.stream()
                .filter(transaction -> Objects.equals(transaction.getSourceAccount(), account))
                .map(Transaction::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        var incomingAmount = accountTransactions.stream()
                .filter(transaction -> Objects.equals(transaction.getTargetAccount(), account))
                .map(Transaction::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return incomingAmount.subtract(outgoingAmount);
    }

    private void checkAccountBalance(Account account, BigDecimal amount) {
        var accountBalance = getAccountBalance(account);
        if(accountBalance.compareTo(amount) < 0) {
            throw new TransactionException("Source account doesn't have enough funds");
        }
    }

    private void validateAmount(BigDecimal amount) {
        if(amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new TransactionException("Negative amount value!");
        }
    }

    private Transaction createTransaction(TransactionType transactionType) {
        var transaction = new Transaction();
        transaction.setTransactionType(transactionType);
        transaction.setTransactionTime(OffsetDateTime.now());
        return transaction;
    }
}
