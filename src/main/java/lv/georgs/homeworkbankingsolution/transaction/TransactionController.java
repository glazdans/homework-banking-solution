package lv.georgs.homeworkbankingsolution.transaction;

import lombok.AllArgsConstructor;
import lv.georgs.homeworkbankingsolution.account.AccountService;
import lv.georgs.homeworkbankingsolution.model.Account;
import lv.georgs.homeworkbankingsolution.model.BankUser;
import lv.georgs.homeworkbankingsolution.transaction.dto.MoneyRequestDto;
import lv.georgs.homeworkbankingsolution.transaction.dto.TransferRequestDto;
import lv.georgs.homeworkbankingsolution.user.UserService;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transaction")
@AllArgsConstructor
public class TransactionController {
    private final TransactionService transactionService;
    private final AccountService accountService;
    private final UserService userService;

    @PutMapping("/deposit")
    public void deposit(@RequestBody MoneyRequestDto request){
        var currentBankUser = userService.getCurrentUser();
        accountService.checkIfUserHasAccount(currentBankUser, request.getAccountNumber());

        var userAccount = accountService.getAccount(request.getAccountNumber());
        transactionService.deposit(userAccount, request.getAmount());
    }

    @PutMapping("/withdraw")
    public void withdraw(@RequestBody MoneyRequestDto request){
        var currentBankUser = userService.getCurrentUser();
        accountService.checkIfUserHasAccount(currentBankUser, request.getAccountNumber());

        var userAccount = accountService.getAccount(request.getAccountNumber());
        transactionService.withdraw(userAccount, request.getAmount());
    }

    @PutMapping("/transfer")
    public void transfer(@RequestBody TransferRequestDto request){
        var currentBankUser = userService.getCurrentUser();
        accountService.checkIfUserHasAccount(currentBankUser, request.getSourceAccountNumber());

        var sourceAccount = accountService.getAccount(request.getSourceAccountNumber());
        var targetAccount = accountService.getAccount(request.getTargetAccountNumber());
        transactionService.transfer(sourceAccount, targetAccount, request.getAmount());
    }

}
