package lv.georgs.homeworkbankingsolution.transaction.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransferRequestDto {
    private String sourceAccountNumber;
    private String targetAccountNumber;
    private BigDecimal amount;
}
