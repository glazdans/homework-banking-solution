package lv.georgs.homeworkbankingsolution.transaction.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MoneyRequestDto {
    private String accountNumber;
    private BigDecimal amount;
}
