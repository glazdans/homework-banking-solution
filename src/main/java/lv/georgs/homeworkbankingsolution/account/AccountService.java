package lv.georgs.homeworkbankingsolution.account;

import lombok.AllArgsConstructor;
import lv.georgs.homeworkbankingsolution.model.Account;
import lv.georgs.homeworkbankingsolution.model.BankUser;
import lv.georgs.homeworkbankingsolution.transaction.TransactionService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;
    private final TransactionService transactionService;

    public Account getAccount(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber);
    }

    @Transactional
    public Account createAccount(BankUser bankUser, String name, BigDecimal initialAmount){
        var account = initializeNewAccount(bankUser, name);
        transactionService.deposit(account, initialAmount);
        return account;
    }

    public void checkIfUserHasAccount(BankUser bankUser, String accountNumber) {
        Optional<String> foundUserAccountNumber = bankUser.getAccountList().stream()
                .map(Account::getAccountNumber)
                .filter(nr -> Objects.equals(nr, accountNumber))
                .findAny();

        if(foundUserAccountNumber.isEmpty()){
            throw new InvalidAccountException("User doesn't have account: " + accountNumber);
        }
    }

    private Account initializeNewAccount (BankUser bankUser, String name) {
        var account = new Account();
        account.setName(name);
        account.setAccountNumber(generateAccountNumber());
        account.setCreationDate(OffsetDateTime.now());
        account.setBankUser(bankUser);
        return accountRepository.save(account);
    }

    private String generateAccountNumber() {
        var accountNumber = "LV" +
                RandomStringUtils.randomNumeric(2) +
                "HABA" + RandomStringUtils.randomNumeric(13);

        // validateUniqueness(accountNumber);

        return accountNumber;
    }

}
