package lv.georgs.homeworkbankingsolution.account.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountCreationRequestDto {
    private String accountName;
    private BigDecimal initialDepositAmount;
}
