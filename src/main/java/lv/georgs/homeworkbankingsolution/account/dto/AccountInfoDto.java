package lv.georgs.homeworkbankingsolution.account.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountInfoDto {
    private String accountNumber;
    private String accountName;
    private BigDecimal accountBalance;
}
