package lv.georgs.homeworkbankingsolution.account;

import lombok.AllArgsConstructor;
import lv.georgs.homeworkbankingsolution.account.dto.AccountInfoDto;
import lv.georgs.homeworkbankingsolution.model.Account;
import lv.georgs.homeworkbankingsolution.model.BankUser;
import lv.georgs.homeworkbankingsolution.transaction.TransactionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AccountInfoService {
    private final AccountRepository accountRepository;
    private final TransactionService transactionService;

    public AccountInfoDto getAccountInfo(String accountNumber) {
        var account = accountRepository.findByAccountNumber(accountNumber);
        return getAccountInfo(account);
    }

    public AccountInfoDto getAccountInfo(Account account) {
        var dto = createDtoFrom(account);
        dto.setAccountBalance(transactionService.getAccountBalance(account));
        return dto;
    }

    public List<AccountInfoDto> getUserAccounts(BankUser bankUser) {
        return bankUser.getAccountList().stream()
                .map(this::getAccountInfo)
                .collect(Collectors.toList());
    }

    private AccountInfoDto createDtoFrom(Account account) {
        var dto = new AccountInfoDto();
        dto.setAccountName(account.getName());
        dto.setAccountNumber(account.getAccountNumber());
        return dto;
    }

}
