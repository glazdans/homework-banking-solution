package lv.georgs.homeworkbankingsolution.account;

import lombok.AllArgsConstructor;
import lv.georgs.homeworkbankingsolution.account.dto.AccountCreationRequestDto;
import lv.georgs.homeworkbankingsolution.account.dto.AccountInfoDto;
import lv.georgs.homeworkbankingsolution.user.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/account")
@AllArgsConstructor
public class AccountController {
    private final AccountService accountService;
    private final AccountInfoService accountInfoService;
    private final UserService userService;

    @PutMapping
    public AccountInfoDto createNewAccount(@RequestBody AccountCreationRequestDto request){
        var currentBankUser = userService.getCurrentUser();
        var createdAccount = accountService.createAccount(currentBankUser, request.getAccountName(), request.getInitialDepositAmount());
        return accountInfoService.getAccountInfo(createdAccount);
    }

    @GetMapping("/{accountNumber}")
    public AccountInfoDto getAccountDetails(@PathVariable String accountNumber){
        var currentBankUser = userService.getCurrentUser();
        accountService.checkIfUserHasAccount(currentBankUser, accountNumber);
        return accountInfoService.getAccountInfo(accountNumber);
    }

    @GetMapping
    public List<AccountInfoDto> getAllUserAccounts(){
        var currentBankUser = userService.getCurrentUser();
        return accountInfoService.getUserAccounts(currentBankUser);
    }
}
