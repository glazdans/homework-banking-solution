package lv.georgs.homeworkbankingsolution;

import com.fasterxml.jackson.databind.ObjectMapper;
import lv.georgs.homeworkbankingsolution.account.dto.AccountCreationRequestDto;
import lv.georgs.homeworkbankingsolution.account.dto.AccountInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.math.BigDecimal;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Component
public class AccountActions {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private TestResponseParser responseParser;
    @Autowired
    protected ObjectMapper objectMapper;

    public List<AccountInfoDto> getUserAccounts() throws Exception {
        MvcResult result = mvc.perform(get("/account")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String contentString = result.getResponse().getContentAsString();
        return responseParser.parseList(contentString, AccountInfoDto.class);
    }

    public void createAccount(String accountName, BigDecimal initialBalance) throws Exception {
        AccountCreationRequestDto request = new AccountCreationRequestDto();
        request.setAccountName(accountName);
        request.setInitialDepositAmount(initialBalance);

        mvc.perform(put("/account")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    public AccountInfoDto getAccountByAccountNumber(String accountNumber) throws Exception {
        return getAccountByAccountNumber(accountNumber, status().isOk());
    }

    public AccountInfoDto getAccountByAccountNumber(String accountNumber, ResultMatcher status) throws Exception {
        MvcResult result = mvc.perform(get("/account/" + accountNumber)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status)
                .andReturn();

        String contentString = result.getResponse().getContentAsString();
        return responseParser.parseResponse(contentString, AccountInfoDto.class);
    }

}
