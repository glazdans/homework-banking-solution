package lv.georgs.homeworkbankingsolution;

import lv.georgs.homeworkbankingsolution.account.AccountRepository;
import lv.georgs.homeworkbankingsolution.transaction.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseApplicationTest {
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private AccountRepository accountRepository;

    @BeforeEach
    void cleanUpAccounts() {
        transactionRepository.deleteAll();
        accountRepository.deleteAll();
    }

}
