package lv.georgs.homeworkbankingsolution;

import com.fasterxml.jackson.databind.ObjectMapper;
import lv.georgs.homeworkbankingsolution.transaction.dto.MoneyRequestDto;
import lv.georgs.homeworkbankingsolution.transaction.dto.TransferRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Component
public class TransactionActions {
    @Autowired
    private MockMvc mvc;
    @Autowired
    protected ObjectMapper objectMapper;

    public void deposit(String accountNumber, BigDecimal amount) throws Exception {
        deposit(accountNumber, amount, status().isOk());
    }
    public void deposit(String accountNumber, BigDecimal amount, ResultMatcher status) throws Exception {
        MoneyRequestDto request = new MoneyRequestDto();
        request.setAccountNumber(accountNumber);
        request.setAmount(amount);

        mvc.perform(put("/transaction/deposit")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status);
    }

    public void withdraw(String accountNumber, BigDecimal amount) throws Exception {
        withdraw(accountNumber, amount, status().isOk());
    }

    public void withdraw(String accountNumber, BigDecimal amount, ResultMatcher status) throws Exception {
        MoneyRequestDto request = new MoneyRequestDto();
        request.setAccountNumber(accountNumber);
        request.setAmount(amount);

        mvc.perform(put("/transaction/withdraw")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status);
    }

    public void transfer(String sourceAccountNumber, String targetAccountNumber, BigDecimal amount) throws Exception {
        transfer(sourceAccountNumber, targetAccountNumber, amount, status().isOk());
    }
    public void transfer(String sourceAccountNumber, String targetAccountNumber, BigDecimal amount, ResultMatcher status) throws Exception {
        TransferRequestDto request = new TransferRequestDto();
        request.setSourceAccountNumber(sourceAccountNumber);
        request.setTargetAccountNumber(targetAccountNumber);
        request.setAmount(amount);

        mvc.perform(put("/transaction/transfer")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status);
    }
}
