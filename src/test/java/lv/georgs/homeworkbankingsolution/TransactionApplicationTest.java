package lv.georgs.homeworkbankingsolution;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TransactionApplicationTest extends BaseApplicationTest{
    @Autowired
    private AccountActions accountActions;
    @Autowired
    private TransactionActions transactionActions;

    @Test
    void testDepositToAccount() throws Exception {
        var accountNumber = createTestAccountAndGetNumber(BigDecimal.ZERO);

        assertBalance(accountNumber, BigDecimal.ZERO);

        transactionActions.deposit(accountNumber, BigDecimal.valueOf(1000));
        assertBalance(accountNumber, BigDecimal.valueOf(1000));

        transactionActions.deposit(accountNumber, BigDecimal.valueOf(3000));
        assertBalance(accountNumber, BigDecimal.valueOf(4000));
    }

    @Test
    void testNegativeDepositValues() throws Exception {
        var accountNumber = createTestAccountAndGetNumber(BigDecimal.ZERO);

        assertBalance(accountNumber, BigDecimal.ZERO);

        transactionActions.deposit(accountNumber, BigDecimal.valueOf(-1000), status().isBadRequest());
    }

    @Test
    void testWithdrawFromAccount() throws Exception {
        var accountNumber = createTestAccountAndGetNumber(BigDecimal.valueOf(1000));

        assertBalance(accountNumber, BigDecimal.valueOf(1000));
        transactionActions.withdraw(accountNumber, BigDecimal.valueOf(300));

        assertBalance(accountNumber, BigDecimal.valueOf(700));
        transactionActions.withdraw(accountNumber, BigDecimal.valueOf(500));

        assertBalance(accountNumber, BigDecimal.valueOf(200));
    }

    @Test
    void testWithdrawFromInsufficientBalanceAccount() throws Exception {
        var accountNumber = createTestAccountAndGetNumber(BigDecimal.valueOf(1000));

        assertBalance(accountNumber, BigDecimal.valueOf(1000));
        transactionActions.withdraw(accountNumber, BigDecimal.valueOf(1500), status().isBadRequest());
    }

    @Test
    void testTransfer() throws Exception {
        var firstAccount = createTestAccountAndGetNumber(BigDecimal.valueOf(1000));
        var secondAccount = createTestAccountAndGetNumber(BigDecimal.valueOf(0));
        assertBalance(firstAccount, BigDecimal.valueOf(1000));
        assertBalance(secondAccount, BigDecimal.valueOf(0));

        transactionActions.transfer(firstAccount, secondAccount, BigDecimal.valueOf(300));
        assertBalance(firstAccount, BigDecimal.valueOf(700));
        assertBalance(secondAccount, BigDecimal.valueOf(300));

        transactionActions.transfer(firstAccount, secondAccount, BigDecimal.valueOf(250));
        assertBalance(firstAccount, BigDecimal.valueOf(450));
        assertBalance(secondAccount, BigDecimal.valueOf(550));

        transactionActions.transfer(secondAccount, firstAccount, BigDecimal.valueOf(50));
        assertBalance(firstAccount, BigDecimal.valueOf(500));
        assertBalance(secondAccount, BigDecimal.valueOf(500));

        transactionActions.transfer(firstAccount, secondAccount, BigDecimal.valueOf(550), status().isBadRequest());
    }

    private void assertBalance(String accountNumber, BigDecimal expetedBalance) throws Exception {
        var account = accountActions.getAccountByAccountNumber(accountNumber);
        var accountBalance = account.getAccountBalance();

        assertThat(accountBalance.doubleValue())
                .isEqualTo(expetedBalance.doubleValue());
    }

    private String createTestAccountAndGetNumber(BigDecimal startingBalance) throws Exception {
        accountActions.createAccount("Test account", startingBalance);
        var userAccounts = accountActions.getUserAccounts();
        return userAccounts.getLast().getAccountNumber();
    }
}
