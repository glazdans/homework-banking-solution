package lv.georgs.homeworkbankingsolution;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class TestResponseParser {

    @Autowired
    protected ObjectMapper objectMapper;

    public <T> T parseResponse(String contentString, Class<T> clazz) {
        return Optional.ofNullable(contentString)
                .filter(content -> content.length() > 0)
                .map(content -> readValue(contentString, clazz))
                .orElse(null);
    }

    private <T> T readValue(String contentString, Class<T> clazz) {
        try {
            return objectMapper.readValue(contentString, clazz);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Cant parse response");
        }
    }

    public <T> List<T> parseList(String contentString, Class<T> clazz) {
        return Optional.ofNullable(contentString)
                .filter(content -> content.length() > 0)
                .map(content -> readListValue(contentString, clazz))
                .orElse(Collections.emptyList());
    }

    private <T> List<T> readListValue(String contentString, Class<T> clazz) {
        try {
            var listType = objectMapper.getTypeFactory().constructCollectionType(List.class, clazz);
            return objectMapper.readValue(contentString, listType);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Cant parse response");
        }
    }
}
