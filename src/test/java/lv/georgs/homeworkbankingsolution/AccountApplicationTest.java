package lv.georgs.homeworkbankingsolution;

import lv.georgs.homeworkbankingsolution.account.dto.AccountInfoDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AccountApplicationTest extends BaseApplicationTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private AccountActions accountActions;

    @Test
    void userCanCreateAccount() throws Exception {
        assertThat(accountActions.getUserAccounts())
                .hasSize(0);

        var account1Name = "Test account 1";
        var account1Balance = BigDecimal.valueOf(1000);
        accountActions.createAccount(account1Name, account1Balance);

        assertThat(accountActions.getUserAccounts())
                .hasSize(1);

        var account2Name = "Test account 2";
        var account2Balance = BigDecimal.valueOf(0);
        accountActions.createAccount(account2Name, account2Balance);

        var userAccounts = accountActions.getUserAccounts();
        assertThat(userAccounts)
                .hasSize(2)
                .satisfiesExactly(
                        account1 -> assertAccount(account1, account1Name, account1Balance),
                        account2 -> assertAccount(account2, account2Name, account2Balance)
                );

        AccountInfoDto accountByNumber = accountActions.getAccountByAccountNumber(userAccounts.get(0).getAccountNumber());
        assertAccount(accountByNumber, account1Name, account1Balance);
    }

    @Test
    void userCantAccessDifferentAccounts() throws Exception {
        var testAccountNumber = "LV20TEST00000000001";
        accountActions.getAccountByAccountNumber(testAccountNumber, status().isBadRequest());
    }

    private void assertAccount(AccountInfoDto account, String name, BigDecimal balance) {
        assertThat(account)
                .extracting(AccountInfoDto::getAccountName, acc -> acc.getAccountBalance().doubleValue())
                .containsExactly(name, balance.doubleValue());
    }



}
